using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using MathDemo;

namespace MathDemo.Tests
{
    public class MathServiceTests
    {
        private readonly MathService _mathService;
      
        public MathServiceTests()
        {
            _mathService = new MathService();
        }
      
        [Theory]
        [InlineData(9, 6, 15)]
        [InlineData(5, -10, -5)] 
        public void AddTheory(int x, int y, int expected)
        {
            int result = _mathService.Add(x, y);
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(9, 6, 3)]
        [InlineData(5, 10, -5)] 
        public void SubstractTheory(int x, int y, int expected)
        {
            int result = _mathService.Subtract(x, y);
            Assert.Equal(expected, result);
        }
    }
}
